const mysql = require('mysql')
const con = mysql.createConnection({
  host: '192.168.10.10',
  database: 'api',
  user: 'admindb',
  password: 'adminpass',
  port: 3306,
})

con.connect(function(error) {
  if (error) throw error;
  console.log("Connected")
})

const getUsers = (request, response) => {
  con.query('SELECT name, email FROM users ORDER BY id ASC', (error, results) => {
    if(error) { throw error }
    response.status(200).json(results)
  })
}

const getUserById = (request, response) => {
  const id = parseInt(request.params.id)

  con.query('SELECT * FROM users WHERE id = ?', [id], (error, results) => {
    if(error) { throw error }
    response.status(200).json(results)
  })
}

const createUser = (request, response) => {
  const { name, email } = request.body

  con.query('INSERT INTO users (name, email) VALUES (?, ?)', [name, email], (error, results) => {
    if(error) { throw error }
      response.status(201).send(`User added with ID: ${results}`)
      // console.log(results.insertId)
  })
}

const updateUser = (request, response) => {
  const id = parseInt(request.params.id)
  const { name, email } = request.body

  con.query(
    'UPDATE users SET name = ?, email = ? WHERE id = ?', [name, email, id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User modified with ID: ${id}`)
    }
  )
}

const deleteUser = (request, response) => {
  const id = parseInt(request.params.id)

  con.query('DELETE FROM users WHERE id = ?', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
}